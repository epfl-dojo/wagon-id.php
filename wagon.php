<?php
function getIDwag ($wagon_number) {
    $wagon_number_arr = str_split(str_replace(' ', '', $wagon_number));
    $keypair = $val = $sum = 0;
    foreach($wagon_number_arr as $digit){
        if($keypair % 2 == 0){
            $val = $digit * 2;
            if($val > 9){
                $val_arr = str_split($val);
                $sum += $val_arr[0] + $val_arr[1];
            } else {
                $sum += $val;
            }
        } else {
            $sum += $digit;
        }
        $keypair += 1;
    }
    $next10 = ceil($sum /10) * 10;
    $res = $next10 - $sum;
    return print $wagon_number."-".$res."\n";
}

getIDwag($argv[1]);

# Test formatif PHP

## Résumé
On veut calculer la somme de contrôle des numéros de wagon de train.

## Information

  * Le numéro d'immatriculation comporte douze chiffres
  * Le dernier numéro est la clé de contrôle

### Le calcul de la clé d'autocontrôle 

1. Multiplier alternativement par 2 et par 1, de la
droite à la gauche, tous les chiffres du numéro de wagon
1. Calculer la somme de tous les chiffres obtenus
1. Calculer la dizaine supérieur de ce résultat
1. La différence entre 3. et 2. donne la clé de contrôle

La notation est alors numéro de wagon, trait d'union, clé de contrôle, e.g. 123456789012-CLE

## Test

Faire l'exervice avec les wagons suivants:
  - `21 81 2471 217`
  - `51 80 08 43 001`